﻿using UnityEngine;
using Zenject;

namespace Installers
{
    [RequireComponent(typeof(Canvas))]
    public class UICameraSetter : MonoBehaviour
    {

        [Inject] private GameServices _services;

        private void Awake()
        {
            GetComponent<Canvas>().worldCamera = _services.Cameras.UI;
        }
    }
}
