using System;
using Features;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "GameSettingsInstaller", menuName = "Installers/GameSettingsInstaller")]
public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
{
    public PlayerController.PlayerSettings Player;

    [Serializable]
    public class GameSettings
    {
    }

    public override void InstallBindings()
    {
        Container.BindInstance(Player).IfNotBound();
    }
}