﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Triggers;
using Features;
using Prime31.StateKit;
using StateMachine;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;


[CreateAssetMenu(fileName = "ProjectScriptableObjectInstaller",
    menuName              = "Game Assets/Installers/Project Scriptable Object Installer")]
public class ProjectScriptableObjectInstaller : Zenject.ScriptableObjectInstaller<ProjectScriptableObjectInstaller>
{
    private GameServices _services;
    
    public override void InstallBindings()
    {
        InstallSaveSystem();
        InstallInterfaceBindings();
        InstallGameManager();
    }

    private void InstallSaveSystem()
    {
        Container.BindInterfacesAndSelfTo<SaveSystemService>().FromInstance(new SaveSystemService()).AsSingle();
    }

    private void InstallInterfaceBindings()
    {
        _services               = new GameServices();
        _services.MainContainer = Container;
        Container.BindInstances(_services);
    }

    private void InstallGameManager()
    {
        Container
            .Bind<GameManager>()
            .FromNewComponentOnNewGameObject()
            .WithGameObjectName("[Game Manager]")
            .AsSingle()
            .NonLazy();
    }
}