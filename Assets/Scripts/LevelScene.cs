﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScene : MonoBehaviour
{
    [System.Serializable]
    public class SceneSetupConfiguration
    {
        public Vector3 CameraPositin;
        public Vector3 CameraRotation;
        public Vector3 Fov;
        public float   LightIntensity;
    }
    

    public enum GaneState
    {
        GameLoading,
        SceneLoading
        
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}