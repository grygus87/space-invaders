﻿using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(menuName = "Game Assets/Asset Group")]
public class AsssetGroup : ScriptableObject
{
    public AssetReference StaticPrefabs;
    public AssetReference Environment;
}
