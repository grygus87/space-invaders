﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchInput : MonoBehaviour
{
    [SerializeField] private Joystick _joystick;
    [SerializeField] private Button _fireButton;
    public Vector2 GetAxis()
    {
        return _joystick.Direction;
    }

    public Action OnFire;

    private void Awake()
    {
        _fireButton.onClick.AddListener(() => OnFire?.Invoke());
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
    
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}