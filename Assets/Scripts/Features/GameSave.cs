﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UIElements;

[System.Serializable]
public class GameSave
{
    public                           List<string> RecordList => ScoresRepresentation;
    [SerializeField] private         List<string> ScoresRepresentation;
    [SerializeField] private         List<int>    Scores;
    [NonSerialized]  public readonly int          MaxRecords = 10;

    public GameSave()
    {
        ScoresRepresentation = new List<string>(10);
        Scores               = new List<int>(10);
    }

    public void AddRecord(int score)
    {
        var index = GetInsertIndex(score);
        if (index > -1)
        {
            Scores.Insert(index, score);
            ScoresRepresentation.Insert(index, GetCurrentRepresentation(score));
            if (Scores.Count > MaxRecords)
                RemoveLastRecord();
        }
        else if (Scores.Count < MaxRecords)
        {
            Scores.Add(score);
            ScoresRepresentation.Add(GetCurrentRepresentation(score));
        }
    }

    public void Validate()
    {
        var arrayDiff = Scores.Count - ScoresRepresentation.Count;
        if (arrayDiff > 0)
        {
            for (int i = ScoresRepresentation.Count; i < Scores.Count; i++)
            {
                ScoresRepresentation.Add(GetCurrentRepresentation(Scores[i]));
            }
        }
        else if (arrayDiff < 0)
        {
            ScoresRepresentation.RemoveRange(Scores.Count, Mathf.Abs(arrayDiff));
        }
    }

    private void RemoveLastRecord()
    {
        Scores.RemoveAt(Scores.Count - 1);
        ScoresRepresentation.RemoveAt(ScoresRepresentation.Count - 1);
    }

    private int GetInsertIndex(int score)
    {
        int index = -1;
        for (int i = 0; i < Scores.Count; i++)
        {
            if (score > Scores[i])
            {
                index = i;
                break;
            }
        }

        return index;
    }

    private string GetCurrentDate()
    {
        return DateTime.Now.ToString("g", CultureInfo.InvariantCulture);
    }

    private string GetCurrentRepresentation(int score)
    {
        return $"{GetCurrentDate()} Score: {score}";
    }
}