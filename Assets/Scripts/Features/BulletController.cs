﻿using System;
using UnityEditor;
using UnityEngine;

namespace Features
{
    public class BulletController : MonoBehaviour
    {
        public enum  EBulletOwner
        {
            Player,
            Enemy
        }
        public  float                    Speed;
        private Transform                _transform;
        private BoxCollider              _collider;
        private Collider[]               _collidersBuffor = new Collider[1];
        public  Action<BulletController> OnHit;
        public  LayerMask                _collisionDetectionLayer;

        [SerializeField] private Vector3 _halfExtents;

        private          float _passedDistance = 0.0f;
        private readonly float _maxDistance    = 22;

        // Start is called before the first frame update
        private void Awake()
        {
            _transform = transform;
            _collider  = GetComponent<BoxCollider>();
        }

        // Update is called once per frame
        void Update()
        {
            UpdatePosition();
            UpdateCollision();
        }

        private void UpdatePosition()
        {
            var dx = Speed * Time.deltaTime;
            _passedDistance     += dx;
            if(_passedDistance >= _maxDistance)
                OnHit?.Invoke(this);
            
            _transform.position += transform.forward * dx;
        }

        private void UpdateCollision()
        {
            var countHit = Physics.OverlapBoxNonAlloc(_transform.position, _halfExtents, _collidersBuffor, _transform.rotation,
                _collisionDetectionLayer);
            if (countHit > 0)
            {
                //Destroy Enemy
                var unit = _collidersBuffor[0].GetComponent<BaseUnit>();
                unit.Hit();
                // GameObject.Destroy(_collidersBuffor[0].gameObject);
                // Destroy This Object
                //
                
                OnHit?.Invoke(this);
            }
            
        }
        
        

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, _halfExtents * 2f);
        }

        public void SetOwner(EBulletOwner owner)
        {
            switch (owner)
            {
                case EBulletOwner.Player:
                    _collisionDetectionLayer = LayerMask.GetMask("EnemyUnit"); 
                    break;
                case EBulletOwner.Enemy:
                    _collisionDetectionLayer = LayerMask.GetMask("PlayerUnit"); 
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(owner), owner, null);
            }

            _passedDistance = 0.0f;
        }
    }
}