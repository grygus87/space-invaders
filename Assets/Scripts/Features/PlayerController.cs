﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Serialization;
using Zenject;

namespace Features
{
    public class PlayerController : BaseUnit
    {
        public  AssetReference Bullet;
        public  float          UnitSpeed;
        public int            Lives;
        public  Vector2        MaxPosition;
        
        private Transform      _transform;
        private Vector3        _position;

        [Inject] private TouchInput     _touchInput;
        [Inject] private GameWorld      _world;
        [Inject] private PlayerSettings _settings;
        public           Action<int>    OnHit;
        public           Action         OnDead;

        private void Start()
        {
            _transform         =  transform;
            _position          =  _transform.position;
            _touchInput.OnFire += () => ShootBullet();
        }

        private void Update()
        {
            var input = _touchInput.GetAxis().x;
            input = (input == 0.0f) ? Input.GetAxis("Horizontal") : input;
            var fire = Input.GetButtonDown("Jump");


            _position.x = Mathf.Clamp(_position.x + input * Time.deltaTime * UnitSpeed,
                MaxPosition.x,
                MaxPosition.y);
            _transform.position = _position;

            if (fire) ShootBullet();
        }

        private async UniTaskVoid ShootBullet()
        {
            _world.SpawnBullet(_position, Quaternion.identity, BulletController.EBulletOwner.Player);
        }

        public void RestartPlayer()
        {
            Lives = _settings.Lives;
        }

        public override void Hit()
        {
            Lives -= 1;
            OnHit?.Invoke(Lives);
            if (Lives == 0)
                DestroyUnit();
        }

        public virtual void DestroyUnit()
        {
            OnDead?.Invoke();
        }

        [Serializable]
        public class PlayerSettings
        {
            public int Lives;
        }
    }
}