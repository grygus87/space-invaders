﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EnemyUnit : BaseUnit
{
    public           Action    OnHit;

    public override void Hit()
    {
        DestroyUnit();
    }

    private void DestroyUnit()
    {
        IsAlive = false;
        OnHit?.Invoke();
        gameObject.SetActive(false);
    }

    public void ActivateUnit()
    {
        IsAlive = true;
        gameObject.SetActive(true);
    }
}