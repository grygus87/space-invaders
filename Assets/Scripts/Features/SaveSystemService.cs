﻿using System;
using System.IO;
using UnityEngine;
using Zenject;

namespace Features
{
    public class SaveSystemService : IInitializable, IDisposable
    {
        public GameSave Data;

        private readonly string _fileName = "HighScores.data";
        private          string _pathToData;

        public void Initialize()
        {
            _pathToData = Path.Combine(Application.persistentDataPath, _fileName);

            if (!File.Exists(_pathToData))
            {
                Data = new GameSave();
            }
            else
            {
                try
                {
                    Data = LoadData(_pathToData);
                    Data.Validate();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Data = new GameSave();
                }
            }
        }

        public void SaveToSystem()
        {
            SaveFile(_pathToData, Data);
        }

        private void SaveFile(string filePath, GameSave save)
        {
            File.WriteAllText(filePath, JsonUtility.ToJson(save));
        }

        private GameSave LoadData(string filePath)
        {
            return JsonUtility.FromJson<GameSave>(File.ReadAllText(filePath));
        }

        public void Dispose()
        {
            SaveToSystem();
        }
    }
}