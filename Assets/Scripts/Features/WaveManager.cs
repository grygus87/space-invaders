﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Features;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Serialization;
using Zenject;
using Random = UnityEngine.Random;

public class WaveManager : MonoBehaviour
{
    public          AssetReference[] UnitInRow;
    public readonly int              NColumns = 11;
    public          int              NRows;
    public          float            delayMultiplier = 1;

    [Header("Unit Displacement")] public float UnitOffset;

    [Inject] private GameWorld             _world;
    private          List<List<EnemyUnit>> _unitColumns;
    private          List<List<EnemyUnit>> _unitRows;
    private          List<Transform>       _rowHandlers;
    private          List<Tweener>         _rowTweeners;
    private          float                 _unitHeight;
    private          float                 _unitWidth;
    private          int                   _activeUnits;
    private          int                   MaxUnits;
    public           Action                WaveDestroyed;
    public           Action                WaveHit;

    private float _shootTimer;
    public  int   _shootFrewuency = 1;
    public  float _nextShootTime;

    [Header("Movement Settings")] public Vector2 StageExtents;

    private void Awake()
    {
        NRows        = UnitInRow.Length;
        _unitColumns = new List<List<EnemyUnit>>(NColumns);
        _unitRows    = new List<List<EnemyUnit>>(NRows);
        _rowHandlers = new List<Transform>(NRows);
        _rowTweeners = new List<Tweener>(NRows);
        InitializeUnits();
    }

    private void Update()
    {
        if (_activeUnits > 0)
        {
            _shootTimer += Time.deltaTime;
            if (_shootTimer >= _nextShootTime)
            {
                _nextShootTime = ((float) _activeUnits / MaxUnits) + 1 / _shootFrewuency;
                _shootTimer    = 0.0f;
                WaveShoot();
            }
        }
    }

    private void WaveShoot()
    {
        var random = Random.Range(0, _unitColumns.Count);
        var column = _unitColumns[random];
        for (int i = 0; i < column.Count; i++)
        {
            var index = column.Count - 1 - i;
            var unit  = column[index];
            if (unit.IsAlive)
            {
                EnemyShoot(unit);
                break;
            }
        }
    }

    private void EnemyShoot(EnemyUnit unit)
    {
        _world.SpawnBullet(
            unit.transform.position
            , Quaternion.LookRotation(Vector3.back,Vector3.up)
            , BulletController.EBulletOwner.Enemy);
    }

    public async UniTask StartMove(CancellationToken cancellationToken = new CancellationToken())
    {
        int horizontalIterations = 12;
        int verticalIterations   = 5;
        int startIteration       = 5;

        for (int i = 0; i < verticalIterations; i++)
        {
            var moveDirection = (i % 2) == 0 ? Vector3.right : Vector3.left;
            for (int j = startIteration; j < horizontalIterations; j++)
            {
                Sequence sequence = DOTween.Sequence();
                for (int k = 0; k < _rowHandlers.Count; k++)
                {
                    var iK = _rowHandlers.Count - 1 - k;
                    sequence.Join(_rowHandlers[iK]
                        .DOBlendableLocalMoveBy(moveDirection * UnitOffset * 0.5f, 0.5f * delayMultiplier)
                        .SetEase(Ease.Linear).SetDelay((0.15f + 0.15f * (k / 2)) * delayMultiplier));
                }

                await sequence.Play().WithCancellation(cancellationToken).SuppressCancellationThrow();
            }

            Sequence downSequence = DOTween.Sequence();
            for (int k = 0; k < _rowHandlers.Count; k++)
            {
                var iK = _rowHandlers.Count - 1 - k;
                downSequence.Join(_rowHandlers[iK]
                    .DOBlendableLocalMoveBy(-Vector3.forward * UnitOffset - moveDirection * UnitOffset * 0.5f,
                        0.5f * delayMultiplier)
                    .SetEase(Ease.Linear).SetDelay((0.15f + 0.15f * (k / 2)) * delayMultiplier));
            }

            await downSequence.Play().WithCancellation(cancellationToken).SuppressCancellationThrow();
            startIteration = 0;
        }

        if (!cancellationToken.IsCancellationRequested)
        {
            _world.OnPlayerDead();
        }
    }

    [ContextMenu("Start Move")]
    private void Move()
    {
        StartMove();
    }

    [ContextMenu("Generate Wave")]
    private void InitializeUnits()
    {
        var x = -NColumns * UnitOffset * 0.5f;
        for (int i = 0; i < NColumns; i++)
        {
            var y = NRows * UnitOffset * 0.5f;
            _unitColumns.Add(new List<EnemyUnit>());
            for (int j = 0; j < NRows; j++)
            {
                var position = transform.position + new Vector3(x, 0, y);
                var unit = UnitInRow[j]
                    .PullFromStorage(position,
                        Quaternion
                            .identity); // .InstantiateAsync(position, Quaternion.identity, transform).ToUniTask();
                unit.SetActive(true);
                var enemyUnit = unit.GetComponent<EnemyUnit>();
                enemyUnit.OnHit += OnHit;
                _unitColumns[i].Add(enemyUnit);
                y -= UnitOffset;
            }

            x += UnitOffset;
        }

        for (int i = 0; i < NRows; i++)
        {
            _unitRows.Add(new List<EnemyUnit>());
            var rowhandler = new GameObject($"Row_{i}").transform;
            rowhandler.SetParent(transform, false);
            _rowHandlers.Add(rowhandler);
            for (int j = 0; j < _unitColumns.Count; j++)
            {
                // if (j == 0)
                // {
                // var position = _unitRows[i][j].transform.position - _unitWidth * 0.5f * Vector3.right -
                // _unitHeight * Vector3.forward * 0.5f;
                // rowhandler.position = position;
                // }

                _unitRows[i].Add(_unitColumns[j][i]);
                _unitRows[i][j].transform.SetParent(rowhandler, true);
            }
        }

        MaxUnits = NColumns * NRows;
    }

    private void OnHit()
    {
        WaveHit?.Invoke();
        _activeUnits--;
        if (_activeUnits == 0)
        {
            WaveDestroyed?.Invoke();
        }
    }

    [ContextMenu("Update Wave")]
    private void SpawnUnits()
    {
        var x = -NColumns * UnitOffset * 0.5f;
        for (int i = 0; i < NColumns; i++)
        {
            var y = NRows * UnitOffset * 0.5f;
            for (int j = 0; j < NRows; j++)
            {
                _unitColumns[i][j].transform.position =  transform.position + new Vector3(x, 0, y);
                y                                     -= UnitOffset;
                _unitColumns[i][j].ActivateUnit();
            }

            x += UnitOffset;
        }

        _activeUnits = NColumns * NRows;
    }

    [ContextMenu("Despawn Wave")]
    private void Despawn()
    {
        for (int i = 0; i < _unitColumns.Count; i++)
        {
            for (int j = 0; j < _unitColumns[i].Count; j++)
            {
                Addressables.ReleaseInstance(_unitColumns[i][j].gameObject);
            }
        }
    }

    public void HideWave()
    {
        gameObject.SetActive(false);
    }

    public void SpawnWave()
    {
        gameObject.SetActive(true);
        SpawnUnits();
    }
}