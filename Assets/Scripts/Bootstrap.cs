﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Grygus.Utilities.Pool;
using Grygus.Utilities.Pool.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using UnityEngine.UI;
using Zenject;

[assembly: Preserve]

public class Bootstrap : MonoBehaviour
{
    public AsssetGroup     Assets;
    public Image           Overlay;
    public TextMeshProUGUI LoadingText;
    public AssetReference  MainScene;
    public GameObject      LoadingCanvas;

    private Tweener _loadingTweener;
    private Tweener _blackScreenTweenrer;

    private Color         _overlayColor;
    private SceneInstance _mainScene;

    [Inject] private DiContainer  _container;
    [Inject] private GameServices _services;
    [Inject] private GameManager  _gameManager;


    private void Awake()
    {
        DOTween.Init();
        StartIntroAnimation();
        _overlayColor     = Overlay.color;
    }


    // Start is called before the first frame update
    async Task Start()
    {
        await LoadBootstrapDependencies();
        await UniTask.WhenAll(
            FadeOut(),
            LoadGame()
        );
        LoadingCanvas.SetActive(false);
        await _mainScene.ActivateAsync();
        var sceneContainer = GetContainerForScene(_mainScene.Scene);
        await _gameManager.StartGameOnScene(sceneContainer);
    }

    private async UniTask FadeOut()
    {
        _blackScreenTweenrer = DOTween.To(() => Overlay.color.a, x =>
        {
            _overlayColor.a = x;
            Overlay.color   = _overlayColor;
        }, 0, 1);
        await _blackScreenTweenrer.Play();
    }

    private void StartIntroAnimation()
    {
        var originalColor = LoadingText.color;
        LoadingText.overrideColorTags = true;
        _loadingTweener = DOTween.To(() => LoadingText.color.a,
                x => LoadingText.color = new Color32((byte) (originalColor.r * 255), (byte) (originalColor.g * 255),
                    (byte) (originalColor.b * 255), (byte) x), 128, 1)
            .SetLoops(-1, LoopType.Yoyo)
            .SetAutoKill(true)
            .Play();
    }

    private async Task LoadBootstrapDependencies()
    {
        //Need Some Tests
        
        /*await Assets.Environment.LoadAssetAsync<GameObject>();
        var envGo = await Assets.Environment.InstantiateAsync();
        envGo.name = envGo.name.Replace("(Clone)", "");
        DontDestroyOnLoad(envGo);*/
        // var component = envGo.GetComponent<ViewController>();
        // _container.BindInstance(envGo.GetComponent<EnvironmentController>());
        // _container.BindInterfacesTo<ViewController>().FromInstance(component).AsSingle();
        // var env = _container.Resolve<IEnvironmentController>();
        // var env2 = _services.MainContainer.Resolve<IEnvironmentController>();

        await Assets.StaticPrefabs.LoadAssetAsync<GameObject>();
        var statGo = await Assets.StaticPrefabs.InstantiateAsync();
        statGo.name = statGo.name.Replace("(Clone)", "");
        await statGo.GetComponent<ResourcesInstaller>().Inject(_services);
        DontDestroyOnLoad(statGo);


        // var bin1 = _services.MainContainer.Resolve<TestBindingMono>();
        // var bin2 = _container.Resolve<TestBindingMono>();
        // var cam1 = _services.MainContainer.Resolve<IGameCamera>();
        // var cam2 = _container.Resolve<IGameCamera>();
        // var env1 = _services.MainContainer.Resolve<IEnvironmentController>();
        // var env2 = _container.Resolve<IEnvironmentController>();
    }

    private async UniTask LoadGame()
    {
        await _services.Storage.Initialize();
        _mainScene = await Addressables.LoadSceneAsync(MainScene, LoadSceneMode.Additive, false);
    }

    private DiContainer GetContainerForScene(Scene scene)
    {
        DiContainer container = null;
        foreach (var rootObject in scene.GetRootGameObjects())
        {
            var context = rootObject.GetComponent<SceneContext>();
            if (context)
            {
                container = context.Container;
            }
        }

        return container;
    }
}