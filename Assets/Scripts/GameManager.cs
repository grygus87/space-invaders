﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Features;
using Prime31.StateKit;
using StateMachine;
using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour
{
    public           GameWorld                   CurrentWorld;
    public           string                      ActiveState;
    private          SKStateMachine<GameManager> _machine;
    [Inject] private DiContainer                 _container;
    [Inject] private GameServices                _services;
    private          DiContainer                 _sceneContainer;
    private          bool                        _isGameLoaded;

    private List<SKState<GameManager>> _states = new List<SKState<GameManager>>()
    {
        new GameBootingState(),
        new MenuState(),
        new LoadingLevelState(),
        new PlayingState(),
        new GameOverState(),
        new LoadingMenuState()
    };

    public async UniTask StartGameOnScene(DiContainer sceneContext)
    {
        _sceneContainer = sceneContext;
        InitializeManager(sceneContext);
        await _machine.changeState<MenuState>();
    }

    private void InitializeManager(DiContainer contextContainer)
    {
        _machine = new SKStateMachine<GameManager>(this, _states[0]);
        try
        {
            for (int i = 0; i < _states.Count; i++)
            {
                contextContainer.Inject(_states[i]);
                if (i != 0)
                    _machine.addState(_states[i]);
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }

        _machine.onStateChanged += MachineOnonStateChanged;
        ActiveState             =  _machine.currentState.GetType().Name;
    }

    public async UniTask LoadLevel()
    {
        await _machine.changeState<LoadingLevelState>();
    }

    public async UniTask QuitLevel()
    {
    }


    public async UniTask LoadGameScene()
    {
        if (_isGameLoaded) return;
        
        var arenaRef = _services.Dependencies.GameArena;
        await arenaRef.LoadAssetAsync<GameObject>();
        var arena = await arenaRef.InstantiateAsync();
        CurrentWorld = arena.GetComponent<GameWorld>();
        _sceneContainer.BindInstance(CurrentWorld);
        _sceneContainer.InjectGameObject(arena);
        _isGameLoaded = true;
    }

    private void MachineOnonStateChanged()
    {
        ActiveState = _machine.currentState.GetType().Name;
    }
}