﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public interface IEnvironmentController
{
    UniTask SetLightIntensity0();
    UniTask SetLightIntensity1();
    void EnablePostprocess();
    void DisablePostprocess();
}

public class ViewController : MonoBehaviour, IEnvironmentController
{
    private static readonly  int      ColorIndex = Shader.PropertyToID("_Color");
    [SerializeField] private Material MainWorldMaterial;
    [SerializeField] private Color    OriginalColor;
    [SerializeField] private Volume   PostprocessVolume;
    private                  Bloom    bloom = null;

    private void Awake()
    {
        PostprocessVolume.sharedProfile.TryGet<Bloom>(out bloom);
    }


    public async UniTask SetLightIntensity(float intensity)
    {
        float factor = Mathf.Pow(2, intensity);
        var   color  = new Color(OriginalColor.r * factor, OriginalColor.g * factor, OriginalColor.b * factor);
        await MainWorldMaterial.DOColor(color, 1).Play();
    }


    [ContextMenu("Stop Scrolling")]
    public void StopSurfaceScrolling()
    {
    }

    [ContextMenu("Start Scrolling")]
    public void StartSurfaceScrolling()
    {
    }

    [ContextMenu("Intensity 0")]
    public  async UniTask SetLightIntensity0()
    {
        SetLightIntensity(-1.4f);
    }

    [ContextMenu("Intensity 1")]
    public async UniTask SetLightIntensity1()
    {
        SetLightIntensity(1);
    }

    [ContextMenu("Intensity 2")]
    public void SetLightIntensity2()
    {
        SetLightIntensity(2.8f);
    }

    private void SetIntensityInstant(float intensity)
    {
        MainWorldMaterial.SetColor(ColorIndex, GetIntensityColor(OriginalColor, intensity));
    }

    private Color GetIntensityColor(Color color, float intensity)
    {
        float factor = Mathf.Pow(2, intensity);
        return new Color(color.r * factor, color.g * factor, color.b * factor);
    }

    private void OnDestroy()
    {
        SetIntensityInstant(1);
    }

    public void DisablePostprocess()
    {
        PostprocessVolume.gameObject.SetActive(false);
    }

    public void EnablePostprocess()
    {
        PostprocessVolume.gameObject.SetActive(true);
    }

}