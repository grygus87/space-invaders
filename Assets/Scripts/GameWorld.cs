﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Triggers;
using Features;
using UI;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

public class GameWorld : MonoBehaviour
{
    public                   AssetReference   BulletRef;
    [SerializeField] private WaveManager      _waveManager;
    [SerializeField] private PlayerController _player;
    [Inject] private HudViewModel     _hud;

    public Action WaveCompleted;
    public Action GameOver;

    public int PlayerLives => _player.Lives;
    public int PlayerScore;


    // Start is called before the first frame update
    void Start()
    {
        _waveManager.WaveDestroyed += WaveDestroyed;
        _waveManager.WaveHit       += WaveHit;
        _player.OnHit              += OnPlayerHit;
        _player.OnDead             += OnPlayerDead;
    }

    private void WaveHit()
    {
        PlayerScore += 20;
        _hud.SetScore(PlayerScore);
    }

    public void OnPlayerDead()
    {
        _player.Lives = 0;
        GameOver?.Invoke();
    }

    private void OnPlayerHit(int lives)
    {
        _hud.SetPlayerLives(lives);
    }

    private void WaveDestroyed()
    {
        WaveCompleted?.Invoke();
    }

    public void PrepareWorld()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SpawnBullet(Vector3 position, Quaternion rotation, BulletController.EBulletOwner owner)
    {
        var bullet = BulletRef.PullFromStorage();
        bullet.transform.position = position;
        bullet.transform.rotation = rotation;
        bullet.SetActive(true);
        var bController = bullet.GetComponent<BulletController>();
        bController.OnHit += OnHit;
        bController.SetOwner(owner);
    }

    private void OnHit(BulletController bullet)
    {
        bullet.OnHit -= OnHit;
        BulletRef.PushToStorage(bullet.gameObject);
    }

    public async UniTask StartNewWave(CancellationToken _cancellationToken)
    {
        _waveManager.SpawnWave();
        await _waveManager.StartMove(_cancellationToken);
    }

    public void ResetWorld()
    {
        PlayerScore = 0;
        _player.RestartPlayer();
        _waveManager.HideWave();
    }
}