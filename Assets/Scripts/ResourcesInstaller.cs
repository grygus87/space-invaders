﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Grygus.Utilities.Pool.Unity;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

public class GameServices
{
    public DiContainer        MainContainer;
    public UnityStorage       Storage;
    public GameCameras        Cameras;
    public InGameDependencies Dependencies;
}

public class GameResources
{
}

[System.Serializable]
public class GameCameras
{
    public Camera Main;
    public Camera InGame;
    public Camera UI;
}

[System.Serializable]
public class BootstrapDependencies
{
    public GameCameras     Cameras;
    public TestBindingMono BindingMono;
    public ViewController  EnvironmentController;
}


[System.Serializable]
public class InGameDependencies
{
    public AssetReference GameArena;
    public AssetReference Storage;
}


public class ResourcesInstaller : MonoBehaviour
{
    [SerializeField] private BootstrapDependencies BootResources;
    [SerializeField] private InGameDependencies    InGameResources;

    public async UniTask Inject(GameServices services)
    {
        await InGameResources.Storage.LoadAssetAsync<GameObject>();
        var storageGo = await InGameResources.Storage.InstantiateAsync();
        DontDestroyOnLoad(storageGo);
        services.MainContainer.BindInstance(BootResources.Cameras.Main);
        services.MainContainer.BindInstance(storageGo);
        var cSwitcher = BootResources.Cameras.Main.GetComponent<CameraSwitcher>();
        services.MainContainer.BindInterfacesTo<CameraSwitcher>().FromInstance(cSwitcher).AsSingle();
        services.Storage      = storageGo.GetComponent<UnityStorage>();
        services.Cameras      = BootResources.Cameras;
        services.Dependencies = InGameResources;
        services.MainContainer.BindInstance(BootResources.BindingMono);
        services.MainContainer.BindInterfacesTo<ViewController>().FromInstance(BootResources.EnvironmentController)
            .AsSingle();
    }
}