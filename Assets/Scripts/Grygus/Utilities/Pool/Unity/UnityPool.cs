﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Grygus.Utilities.Pool.Unity
{
    [Serializable]
    public class UnityPool
    {
        public string Name;
        public AssetReference Prefab;
        public bool AllowExpand;
        public bool AllowRecycle;
        public int Size;
        
    }
}
