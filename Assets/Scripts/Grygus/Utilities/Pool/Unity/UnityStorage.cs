﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Grygus.Utilities.Pool;
using Grygus.Utilities.Pool.Unity;
using UnityEngine.AddressableAssets;
using ReferenceMap =
    System.Collections.Generic.Dictionary<string, Grygus.Utilities.Pool.IPool<UnityEngine.GameObject>>;

public static class AssetReferenceExtension
{
    private static ReferenceMap _pools;

    public static GameObject PullFromStorage(this AssetReference reference, Vector3 position, Quaternion rotation)
    {
        var go = PullFromStorage(reference);
        go.transform.position = position;
        go.transform.rotation = rotation;
        return go;
    }

    public static GameObject PullFromStorage(this AssetReference reference)
    {
        if (_pools != null)
        {
            if (_pools.ContainsKey(reference.AssetGUID))
                return _pools[reference.AssetGUID].Pop();
            else
                throw new Exception($"[Pool Exception | Missing Pool] {reference.Asset.name}");
        }

        throw new Exception($"[Pool Exception | Unity Storage UnInitialized] ");
    }

    public static void PushToStorage(this AssetReference reference, GameObject instance)
    {
        if (_pools != null)
        {
            if (_pools.ContainsKey(reference.AssetGUID))
                _pools[reference.AssetGUID].Push(instance);
            else
                throw new Exception($"[Pool Exception | Missing Pool] {reference.Asset.name}");
        }
        else
            throw new Exception($"[Pool Exception | Unity Storage UnInitialized] ");
    }

    public static void Setup(ReferenceMap pools)
    {
        _pools = pools;
    }
}

namespace Grygus.Utilities.Pool.Unity
{
    public class UnityStorage : MonoBehaviour
    {
        public List<UnityPool> Pools
        {
            get { return _pools; }
        }

        [SerializeField] protected List<UnityPool>                 _pools;
        protected                  Cache<GameObject>.CacheRegister _caches;
        protected                  ReferenceMap                    RefToPoolMap;

        protected virtual void Awake()
        {
            GameObject.DontDestroyOnLoad(this);
            RefToPoolMap = new Dictionary<string, IPool<GameObject>>();
        }


        public async UniTask Initialize()
        {
            Cache<GameObject>.Initialize();
            _caches = Cache<GameObject>.Caches;
            for (int i = 0; i < _pools.Count; i++)
            {
                var pool = _pools[i];
                await pool.Prefab.LoadAssetAsync<GameObject>().ToUniTask();
                var parent = new GameObject(pool.Prefab.Asset.name);
                parent.transform.parent = transform;
                var cache = _caches[pool.Prefab.Asset.name];
                await cache.SetFactory(async () =>
                    {
                        // await UniTask.DelayFrame(1);
                        return await pool.Prefab.InstantiateAsync(parent.transform).ToUniTask();
                    })
                    .SetResetAction((item) => { item.SetActive(false); })
                    .Generate(pool.Size);


                if (pool.AllowExpand) cache.AllowExpand();
                if (pool.AllowRecycle) cache.AllowRecycle();

                RefToPoolMap[pool.Prefab.AssetGUID] = cache;
            }

            AssetReferenceExtension.Setup(RefToPoolMap);
        }

        [ContextMenu("TestGenerate")]
        public void TestGenerateOne()
        {
            for (int i = 0; i < _pools.Count; i++)
            {
                var pool = _pools[i];
                _caches[pool.Prefab.Asset.name].Generate(1);
            }
        }
    }
}