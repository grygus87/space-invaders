﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cysharp.Threading.Tasks;
using UnityEngine.Scripting;

namespace Grygus.Utilities.Pool
{
    [Preserve]
    public interface IPool<T>
    {
        int Count { get; }

        event PoolStateChangedEventHandler<T> Added;
        event PoolStateChangedEventHandler<T> Removed;

        UniTask<T>        PopAsync();
        T                 Pop();
        void              Push(T item);
        UniTask<IPool<T>> Generate(int count);
        IPool<T>          SetFactory(Func<UniTask<T>> factoryFunc);
        IPool<T>          SetResetAction(Action<T> resetAction);
        IPool<T>          AllowExpand();
        IPool<T>          DoNotExpand();
        IPool<T>          AllowRecycle();
        IPool<T>          DoNotRecycle();
        IPool<T>          Reset();
        void              Clear();
    }

    public interface IPool
    {
        string Name { get; }
    }
}