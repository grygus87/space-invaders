﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine.Scripting;

namespace Grygus.Utilities.Pool
{
    public delegate void PoolStateChangedEventHandler<T>(IPool<T> sender,T arg);
    [Preserve]
    public static partial class Cache<T> where T : class
    {
        public static CacheRegister Caches;
        private static readonly string DefaultName = "Default";

        public static int Count { get { return DefaultPool.Count; } }
        public static IPool<T> DefaultPool { get; private set; }

        public static event PoolStateChangedEventHandler<T> Added = (sender, arg) => { }; 
        public static event PoolStateChangedEventHandler<T> Removed = (sender, arg) => { };
        public static event PoolStateChangedEventHandler<T> AddedAny = (sender, arg) => { };
        public static event PoolStateChangedEventHandler<T> RemovedAny = (sender, arg) => { };

        public static void Initialize()
        {
            Caches = new CacheRegister();
            DefaultPool = Caches[DefaultName];
            DefaultPool.SetResetAction((instance) => { });
            DefaultPool.Added += (sender, arg) => Added(sender,arg);
            DefaultPool.Removed += (sender, arg) => Removed(sender,arg);
            // CacheLog.AddType(typeof(Cache<T>));
            
        }

        public static void Generate(int count)
        {
            DefaultPool.Generate(count);
        }

        public static void SetFactory(Func<UniTask<T>> factoryMethod)
        {
            DefaultPool.SetFactory(factoryMethod);
        }

        public static void SetResetAction(Action<T> resetMethod)
        {
            DefaultPool.SetResetAction(resetMethod);
        }

        public static UniTask<T> PopAsync()
        {
            return DefaultPool.PopAsync();
        }

        public static T Pop()
        {
            return DefaultPool.Pop();
        }

        public static UniTask<T> PopFrom(string cacheId)
        {
            return Caches[cacheId].PopAsync();
        }

        public static void Push(T instance)
        {
            DefaultPool.Push(instance);
        }

        public static void PushInto(T instance, string cacheId)
        {
            Caches[cacheId].Push(instance);
        }

        public static string GetTypeName()
        {
            return typeof(T).Name;
        }
    }
}
