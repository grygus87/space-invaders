﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using Features;
using Prime31.StateKit;
using UI;
using UnityEngine;
using Zenject;

namespace StateMachine
{
    public class PlayingState : SKState<GameManager>
    {
        [Inject] private IGameCamera                     _gameCamera;
        [Inject] private HudViewModel                    _hud;
        [Inject] private SummaryViewModel                _summary;
        [Inject] private PlayerController.PlayerSettings _settings;
        [Inject] private SaveSystemService               _saveSystem;
        private          CancellationTokenSource         _cancellationToken;
        private          int                             _currentWave;
        private          GameWorld                       _currentWorld;
        private          bool                            _exitRequest;

        public override void onInitialized()
        {
            base.onInitialized();
            _hud.SubscribeOnExit(OnExitGame);
            _summary.SubscribeOnExit(OnExitGame);
        }

        private void OnExitGame()
        {
            _exitRequest = true;
            _cancellationToken.Cancel();
            _context.CurrentWorld.ResetWorld();
            _hud.HideHud();
            _summary.HideSummary();
            _machine.changeState<LoadingMenuState>();
        }

        public override async UniTask begin()
        {
            _exitRequest                =  false;
            _currentWorld               =  _context.CurrentWorld;
            _currentWorld.WaveCompleted += WaveCompleted;
            _currentWorld.GameOver      += GameOver;
            _hud.SetScore(0);
            _hud.SetPlayerLives(_settings.Lives);
            _hud.ShowHud();
            _currentWave = 0;
            while (_currentWorld.PlayerLives > 0 && !_exitRequest)
            {
                _currentWave++;
                _hud.SetWave(_currentWave);
                _cancellationToken = new CancellationTokenSource();
                if(_currentWave > 1)
                    await UniTask.Delay(TimeSpan.FromSeconds(2));
                
                await _context.CurrentWorld
                    .StartNewWave(_cancellationToken.Token)
                    .SuppressCancellationThrow();
            }

            _currentWorld.ResetWorld();
        }

        private void GameOver()
        {
            _cancellationToken.Cancel();

            var finalScore = _currentWorld.PlayerScore;
            _hud.HideHud();
            _summary.SetWave(_currentWave);
            _summary.SetScore(finalScore);
            _summary.ShowSummary();
            _saveSystem.Data.AddRecord(finalScore);
            _saveSystem.SaveToSystem();
        }

        private void WaveCompleted()
        {
            _cancellationToken.Cancel();
        }

        public override void update(float deltaTime)
        {
        }

        public override UniTask end()
        {
            base.end();
            _context.CurrentWorld.WaveCompleted -= WaveCompleted;
            _context.CurrentWorld.GameOver      -= GameOver;
            return UniTask.CompletedTask;
        }
    }
}