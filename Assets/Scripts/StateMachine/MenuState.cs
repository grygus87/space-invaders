﻿using Cysharp.Threading.Tasks;
using Features;
using Prime31.StateKit;
using Zenject;

namespace StateMachine
{
    public class MenuState : SKState<GameManager>
    {
        [Inject] private MenuViewModel     _menu;
        [Inject] private SaveSystemService _saveSystem;

        public override void onInitialized()
        {
            base.onInitialized();
            _menu.SubscribeOnGameEnter(OnGameEnter);
            _menu.SubscribeOnHighScoreEnter(OnHighScoreScreenEnter);
            _menu.SubscribeOnReturnToMenu(OnRetrunToMenu);
        }

        private void OnRetrunToMenu()
        {
            _menu.HideSummaryScreen();
            _menu.ShowMainMenu();
        }

        private void OnHighScoreScreenEnter()
        {
            _menu.HideMineMenu();
            var recordList = _saveSystem.Data.RecordList;
            if (recordList.Count > _saveSystem.Data.MaxRecords)
                recordList = _saveSystem.Data.RecordList.GetRange(0, _saveSystem.Data.MaxRecords);

            _menu.ShowSummaryScreen(recordList);
        }

        private void OnGameEnter()
        {
            _context.LoadLevel();
        }

        public override async UniTask begin()
        {
            _menu.ShowMainMenu();
        }

        public override void update(float deltaTime)
        {
        }

        public override UniTask end()
        {
            _menu.HideMineMenu();
            _menu.HideSummaryScreen();
            return UniTask.CompletedTask;
        }
    }
}