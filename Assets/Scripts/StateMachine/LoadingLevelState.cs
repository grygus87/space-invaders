﻿using Cysharp.Threading.Tasks;
using Prime31.StateKit;
using Zenject;

namespace StateMachine
{
    public class LoadingLevelState : SKState<GameManager>
    {
        [Inject] private IGameCamera            _gameCamera;
        [Inject] private IEnvironmentController _viewController;

        public override async UniTask begin()
        {
            await base.begin();
            await _gameCamera.ShowLoadingView();
            await _viewController.SetLightIntensity0();
            _viewController.DisablePostprocess();
            await _context.LoadGameScene();
            await _machine.changeState<PlayingState>();
        }

        public override void update(float deltaTime)
        {
        }

        public override async UniTask end()
        {
            base.end();
            _gameCamera.ShowGameStage();
        }
    }
}