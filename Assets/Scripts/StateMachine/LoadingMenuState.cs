﻿using Cysharp.Threading.Tasks;
using Prime31.StateKit;
using Zenject;

namespace StateMachine
{
    public class LoadingMenuState : SKState<GameManager>
    {
        [Inject] private IGameCamera            _gameCamera;
        [Inject] private IEnvironmentController _viewController;

        public override async UniTask begin()
        {
            await base.begin();
            await _gameCamera.ShowLoadingView();
            await _viewController.SetLightIntensity1();
            _viewController.EnablePostprocess();
            await _machine.changeState<MenuState>();
        }

        public override void update(float deltaTime)
        {
        }

        public override async UniTask end()
        {
            base.end();
            await _gameCamera.ShowMenu();
        }
    }
}
