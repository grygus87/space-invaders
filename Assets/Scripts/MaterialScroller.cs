﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialScroller : MonoBehaviour
{
    private                 Material _mat;
    private                 float    offset;
    private static readonly int      Offset = Shader.PropertyToID("Vector1_2060D8D1");

    private void Awake()
    {
        _mat = GetComponent<MeshRenderer>().sharedMaterial;
    }

    private void Update()
    {
        offset += Time.deltaTime * 0.35f;
        offset %= 1.0f;
        _mat.SetFloat(Offset, offset);
    }
}