﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Features;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Zenject;

public class MenuViewModel : MonoBehaviour
{
    [SerializeField] private Button _startGameButton;
    [SerializeField] private Button _highScoresButton;
    [SerializeField] private Button _returnToMenuButton;


    [SerializeField] private Canvas MaineMenuCanvas;
    [SerializeField] private Canvas HighScoreCanvas;

    [SerializeField] private TextMeshProUGUI RecordsView;
    private                  RectTransform   RecordsViewRect;

    private void Awake()
    {
        RecordsViewRect = RecordsView.GetComponent<RectTransform>();
    }

    public void HideSummaryScreen()
    {
        HighScoreCanvas.enabled = false;
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void ShowSummaryScreen(List<string> records)
    {
        string output = string.Empty;
        for (int i = 0; i < records.Count; i++)
        {
            var index = 1 + i;
            if (i == records.Count - 1)
            {
                if (i > 8)
                    output += $"{index}. {records[i]}";
                else
                    output += $"{index}.  {records[i]}";
            }
            else
                output += $"{index}.  {records[i]}\n";
        }

        var sizeDelta = RecordsViewRect.sizeDelta;
        sizeDelta.y               = records.Count * 40;
        RecordsViewRect.sizeDelta = sizeDelta;

        RecordsView.text        = output;
        HighScoreCanvas.enabled = true;
    }

    public void ShowMainMenu()
    {
        MaineMenuCanvas.enabled = true;
    }

    public void HideMineMenu()
    {
        MaineMenuCanvas.enabled = false;
    }

    public void SubscribeOnGameEnter(UnityAction onEnter)
    {
        _startGameButton.onClick.AddListener(onEnter);
    }

    public void SubscribeOnHighScoreEnter(UnityAction onEnter)
    {
        _highScoresButton.onClick.AddListener(onEnter);
    }

    public void SubscribeOnReturnToMenu(UnityAction onReturn)
    {
        _returnToMenuButton.onClick.AddListener(onReturn);
    }
}