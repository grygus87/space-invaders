﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    public class HudViewModel : MonoBehaviour
    {
        [Header("View Elements")] 
        [SerializeField] private Button _exitButton;
        [SerializeField] private TextMeshProUGUI _playerLives;
        [SerializeField] private TextMeshProUGUI _playerScore;
        [SerializeField] private TextMeshProUGUI _currentWave;
        [SerializeField] private Canvas _hudCanvas;

        public void SetPlayerLives(int lives)
        {
            _playerLives.text = lives.ToString();
        }

        public void SetWave(int wave)
        {
            _currentWave.text = wave.ToString();
        }

        public void SetScore(int score)
        {
            _playerScore.text = score.ToString();
        }

        public void SubscribeOnExit(UnityAction onExit)
        {
            _exitButton.onClick.AddListener(onExit);
        }

        public void ShowHud()
        {
            _hudCanvas.enabled = true;
        }

        public void HideHud()
        {
            _hudCanvas.enabled = false;
        }
    
    }
}