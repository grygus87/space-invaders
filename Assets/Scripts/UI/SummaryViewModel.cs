﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    public class SummaryViewModel : MonoBehaviour
    {
        [SerializeField] private Button          _exitButton;
        [SerializeField] private TextMeshProUGUI _playerScore;
        [SerializeField] private TextMeshProUGUI _currentWave;
        [SerializeField] private Canvas          _summaryCanvas;

        public void SetWave(int wave)
        {
            _currentWave.text = wave.ToString();
        }

        public void SetScore(int score)
        {
            _playerScore.text = score.ToString();
        }

        public void ShowSummary()
        {
            _summaryCanvas.enabled = true;
        }

        public void HideSummary()
        {
            _summaryCanvas.enabled = false;
        }

        public void SubscribeOnExit(UnityAction onExit)
        {
            _exitButton.onClick.AddListener(onExit);
        }
    }
}