﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Assets/Camera Settings")]
public class CameraSettings : ScriptableObject
{
    public CameraPreset SelectedPreset
    {
        get
        {
            if (Presets.Count > 0)
            {
                Selected = Mathf.Clamp(Selected, 0, Presets.Count - 1);
                return Presets[Selected];
            }
            else return new CameraPreset();
        }
    }

    public List<CameraPreset> Presets;
    public int                Selected;

    public void Remove(int i)
    {
        Debug.Assert(i < Presets.Count && i > 0);
        Presets.RemoveAt(i);
        if(i >= Presets.Count)
            SelectNext();
    }

    public void SelectNext()
    {
        Selected = (Selected + 1) % Presets.Count;
    }
}