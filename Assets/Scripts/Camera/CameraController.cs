﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    //TODO: Think how to remove redundant code but still to make it work in editor mode
    public float FieldOfView => !_camera ? GetComponent<Camera>().fieldOfView : _camera.fieldOfView;
    private Camera _camera;
    
    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    /// <summary>
    /// Passing Rotation around x and y Axis 
    /// </summary>
    /// <param name="rotation"></param>
    public async UniTask ApplyRotation(Vector2 rotation,float duration)
    {
        await transform.DORotate(rotation, duration).Play();
    }

    public async UniTask ApplyPosition(Vector3 position,float duration)
    {
        await transform.DOMove(position, duration).SetEase(Ease.InQuad).Play();
    }

    public async UniTask ApplyFieldOfView(float newFieldOfView)
    {
        await _camera.DOFieldOfView(newFieldOfView, 1).Play();
    }

    
}
