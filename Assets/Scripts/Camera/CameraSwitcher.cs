﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Serialization;

public interface IGameCamera
{
    UniTask ShowLoadingView();
    UniTask ShowGameStage();
    UniTask ShowMenu();
}

[RequireComponent(typeof(Camera), typeof(CameraController))]
public class CameraSwitcher : MonoBehaviour, IGameCamera
{
    public CameraSettings Presets;
    public int            GameStagePresetId;
    public int            MenuPresetId;
    public int            LoadingViewPresetId;

    [ContextMenu("Capture")]
    public void CaptureSettings()
    {
        var controller = GetComponent<CameraController>();

        var controllerTransform = controller.transform;
        var controllerRotation  = controllerTransform.rotation.eulerAngles;
        Presets.Presets.Add(new CameraPreset()
        {
            Position    = controllerTransform.position,
            Rotation    = new Vector2(controllerRotation.x, controllerRotation.y),
            FieldOfView = controller.FieldOfView
        });
    }

    public async UniTask Setup()
    {
        var selectedProfile = Presets.SelectedPreset;

        var controller = GetComponent<CameraController>();
        await UniTask.WhenAll(
            controller.ApplyRotation(selectedProfile.Rotation,selectedProfile.RotationTransitionTIme),
            controller.ApplyPosition(selectedProfile.Position,selectedProfile.PositionTransitionTime),
            controller.ApplyFieldOfView(selectedProfile.FieldOfView)
        );
    }

    public async UniTask ShowLoadingView()
    {
        Presets.Selected = LoadingViewPresetId;
        await Setup();
    }

    public async UniTask ShowGameStage()
    {
        Presets.Selected = GameStagePresetId;
        await Setup();
    }

    public async UniTask ShowMenu()
    {
        Presets.Selected = MenuPresetId;
        await Setup();
    }
}