﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(CameraSwitcher))]
public class CameraSwitcherInspector : Editor
{
    private CameraSwitcher _target;
    private CameraSettings _presets;

    private void OnEnable()
    {
        _target  = target as CameraSwitcher;
        _presets = _target.Presets;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (_presets && _presets.Presets.Count > 0)
        {
            EditorGUILayout.Separator();
            for (int i = 0; i < _presets.Presets.Count; i++)
            {
                var profile = _presets.Presets[i];
                EditorGUILayout.BeginHorizontal();
                profile.Name = EditorGUILayout.TextField("Name", profile.Name);
                if (GUILayout.Button("Apply", GUILayout.Width(100)))
                {
                    _presets.Selected = i;
                    _target.Setup();
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.FloatField("Field Of View", profile.FieldOfView);
                if (GUILayout.Button("Remove", GUILayout.Width(100)))
                {
                    _presets.Remove(i);
                    _target.Setup();
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.Vector3Field("Position", profile.Position);

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.Vector2Field("Rotation", profile.Rotation);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Separator();
            }

            EditorGUILayout.LabelField("Selected: " + _presets.SelectedPreset.Name);
            if (GUILayout.Button("Next"))
            {
                _presets.SelectNext();
                _target.Setup();
            }
        }
    }
}