﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[System.Serializable]
public class CameraPreset
{
    public string  Name;
    public float   FieldOfView;
    public Vector3 Position;
    public Vector2 Rotation;
    public float   PositionTransitionTime;
    public float   RotationTransitionTIme;
}